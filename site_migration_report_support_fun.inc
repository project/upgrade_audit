<?php

/**
 * @file
 * Support functions related to the report generation.
 */

/**
 * Get  migration element details.
 */
function smr_get_general_site_detail() {

  static $arr_site_report = [];
  if (!empty($arr_site_report)) {
    return $arr_site_report;
  }
  // Get site detail.
  $arr_site_report['site_detail'] = array();
  $arr_site_report['site_detail']['domain'] = '';
  $arr_site_report['site_detail']['panthion_id'] = '';
  $arr_site_report['site_detail']['db_size'] = '';
  $arr_site_report['site_detail']['type'] = '';

  // Site information.
  $cmd_options = [];
  $results = drush_invoke_process('@self', 'status', NULL, $cmd_options, ['output' => FALSE]);
  $arr_site_report['site_information']['files'] = $results['object']['files'];

  // For file directory size.
  $command = "du -sh " . $arr_site_report['site_information']['files'];
  $str_file_size = shell_exec($command);
  $arr_file_size = explode('sites', $str_file_size);
  $arr_site_report['site_information']['file_directory_size'] = trim($arr_file_size[0]);

  $arr_site_report['site_information']['drupal-version'] = $results['object']['drupal-version'];

  $arr_site_report['site_name'] = $site_name;

  // User role.
  $user_roles = user_roles();
  foreach ($user_roles as $rid => $role) {
    $arr_site_report['user']['roles'][$rid]['name'] = $role;
    $arr_site_report['user']['roles'][$rid]['user_count'] = db_query("SELECT count(u.uid) AS userCount FROM {users} u INNER JOIN {users_roles} ur ON u.uid = ur.uid WHERE ur.rid =" . $rid)->fetchObject()->userCount;
  }

  // Permissions.
  $permissions = module_invoke_all('permission');
  $arr_site_report['user']['permissions'] = $permissions;

  // Content type object.
  $conten_type_detail = migration_audit_drush_content_type_list();
  foreach ($conten_type_detail as $mech_name => $detail) {
    $arr_site_report['content_types'][$mech_name]['name'] = $detail->name;
    $arr_site_report['content_types'][$mech_name]['description'] = $detail->description;
  }
  $arr_site_report['content_types_fields'] = array();

  $arr_site_report['total_published_node_count'] = 0;
  $arr_site_report['webform'] = array();
  // Get node published.
  foreach ($arr_site_report['content_types'] as $mechine_name => $content_type) {
    $arr_site_report['content_types_fields'][$mechine_name] = array_keys(field_info_instances("node", $mechine_name));
    $nodes = node_load_multiple(array(), array('type' => $mechine_name));
    $count = 0;
    foreach ($nodes as $nid => $node) {
      if ($node->status == 1) {
        $count++;
      }
      if ($mechine_name == 'webform') {

        $total_submission_count = db_query("SELECT count( sid ) AS totalSubmissions FROM `webform_submissions` WHERE nid = " . $nid)->fetchObject()->totalSubmissions;
        $arr_site_report['webform'][$nid] = array(
          'name' => check_plain($node->title),
          'submission' => $total_submission_count,
        );

      }
    }
    $arr_site_report['node_published'][$mechine_name] = $count;
    $arr_site_report['total_published_node_count'] += $arr_site_report['node_published'][$mechine_name];
  }

  // View list.
  $arr_site_report['views'] = array();
  if (function_exists('views_get_all_views')) {
    $arr_views_list = views_get_all_views();
    foreach ($arr_views_list as $view) {
      if (!$view->disabled) {
        $arr_site_report['views'][$view->name] = array(
          'human_name' => $view->human_name,
          'description' => $view->description,
        );
        foreach ($view->display as $display => $data) {
          $arr_site_report['views'][$view->name]['display'][] = $display;
        }
      }
    }
  }

  // Taxonomy.
  $vocabulary = taxonomy_get_vocabularies();
  $arr_site_report['taxonomies'] = array();
  foreach ($vocabulary as $item) {
    $arr_site_report['taxonomies'][$item->machine_name] = array('human_name' => $item->name);
    $count = db_query("SELECT COUNT(tid) AS totalTaxonomy FROM {taxonomy_term_data} WHERE vid = " . $item->vid)->fetchObject()->totalTaxonomy;
    $arr_site_report['taxonomies'][$item->machine_name]['total_term'] = $count;
  }

  // Upload file information.
  $arr_site_report['totalFileCount'] = db_query("SELECT COUNT(fid) AS totalFiles FROM {file_managed} WHERE status = 1")->fetchObject()->totalFiles;

  // Theme detail.
  $cmd_options = [];
  $results = drush_invoke_process('@self', 'status theme', NULL, $cmd_options, ['output' => FALSE]);

  $default_theme_mech_name = $results['object']['theme'];
  $arr_site_report['theme']['default']['mech_name'] = $default_theme_mech_name;
  $arr_site_report['theme']['active_admin_theme'] = $results['object']['admin-theme'];

  // Theme list creation.
  $cmd_options = [
    'type' => 'theme',
    'status' => 'enabled',
    'format' => 'list',
    'quiet' => 1,
  ];

  $results = drush_invoke_process('@self', 'pm-list', NULL, $cmd_options, ['output' => FALSE]);
  // Theme list.
  foreach ($results['object'] as $theme => $info) {
    $theme_path_query = db_query('SELECT filename FROM system WHERE  type = \'theme\' AND name = \'' . $theme . '\';');
    $theme_path = $theme_path_query->fetchObject()->filename;
    $arr_site_report['theme']['list'][$theme] = array(
      'name' => $theme,
      'path' => $theme_path,
    );
  }

  // Theme file detail.
  $default_theme_path = str_replace($default_theme_mech_name . '.info', '', $arr_site_report['theme']['list'][$default_theme_mech_name]['path']);

  $template_file_path = $default_theme_path . 'template.php';
  if (file_exists($template_file_path)) {
    $content_template_file = file_get_contents($template_file_path);
    $arr_site_report['theme']['default']['template.php']['lines_count'] = substr_count($content_template_file, "\n");
    $re = '/function ([a-zA-Z_{1}][a-zA-Z0-9_]+)(?=\()/m';
    preg_match_all($re, $content_template_file, $matches, PREG_SET_ORDER, 0);
    $arr_site_report['theme']['default']['template.php']['file_function'] = array();
    foreach ($matches as $fun) {
      $arr_site_report['theme']['default']['template.php']['file_function'][] = $fun[1];
    }
  }

  // Get base theme, regions, css, js files.
  $info_path = $arr_site_report['theme']['list'][$default_theme_mech_name]['path'];
  if (file_exists($info_path)) {
    $content_info_file = file_get_contents($info_path);
    $arr_info = explode("\n", $content_info_file);
    $arr_site_report['theme']['default']['base_theme'] = '';
    $arr_site_report['theme']['default']['regions'] = array();
    $arr_site_report['theme']['default']['css'] = array();
    $arr_site_report['theme']['default']['js'] = array();
    foreach ($arr_info as $info_line) {

      // Base theme.
      if (strpos(strtolower($info_line), 'base theme') !== FALSE) {
        $arr_site_report['theme']['default']['base_theme'] = trim(explode('=', $info_line)[1]);
      }
      // Region.
      if (strpos(strtolower($info_line), 'regions') !== FALSE) {
        $arr_site_report['theme']['default']['regions'][] = str_replace(']', '', explode('[', explode('=', $info_line)[0])[1]);
      }

      // CSS.
      if (strpos(strtolower($info_line), 'stylesheets') !== FALSE) {
        $arr_site_report['theme']['default']['css'][] = trim(explode('=', $info_line)[1]);
      }

      // JS.
      if (strpos(strtolower($info_line), 'scripts') !== FALSE) {
        $arr_site_report['theme']['default']['js'][] = trim(explode('=', $info_line)[1]);
      }
    }
  }

  // Find number of tpl files.
  $folder = $default_theme_path;
  $command = "find " . $folder . " -name '*.tpl.php'";
  $tpl_files = shell_exec($command);
  $count_tpl_files = substr_count($tpl_files, '.tpl.php');
  $arr_site_report['theme']['default']['tplfiles']['count'] = $count_tpl_files;
  $arr_site_report['theme']['default']['tplfiles']['files'] = $tpl_files;

  // Block.
  $query = db_select('block', 'b')
    ->condition('theme', $default_theme_mech_name)
    ->condition('status', 1)
    ->condition('region', -1, '<>');

  $query->addExpression("CONCAT(module, '_', delta)", 'delta');

  $arr_site_report['theme']['default']['block'] = $query->execute()->fetchCol();

  // Feature.
  $cmd_options = [
    'status' => 'enabled',
    'format' => 'list',
    'quiet' => 1,
  ];

  $results = drush_invoke_process('@self', 'features-list', NULL, $cmd_options, ['output' => FALSE]);
  $arr_site_report['features'] = $results['object'];

  // Module list creation.
  $cmd_options = [
    'type' => 'module',
    'status' => 'enabled',
    'no-core' => 0,
    'format' => 'list',
    'quiet' => 1,
  ];

  $results = drush_invoke_process('@self', 'pm-list', NULL, $cmd_options, ['output' => FALSE]);

  $total = count($results['object']);
  $arr_site_report['modules']['total_count'] = $total;
  // Module list.
  foreach ($results['object'] as $module => $info) {

    $module_path_query = db_query('SELECT filename FROM system WHERE  type = \'module\' AND name = \'' . $module . '\';');
    $module_path = $module_path_query->fetchObject()->filename;

    $module_type = 'undefined';
    if (strpos($module_path, 'features/') !== FALSE) {
      $module_type = 'features';
      continue;
    }
    elseif (strpos($module_path, 'contrib/') !== FALSE) {
      $module_type = 'contrib';
    }
    elseif (strpos($module_path, 'custom/') !== FALSE) {
      $module_type = 'custom';
    }
    elseif (strpos($module_path, 'sites/') === FALSE) {
      $module_type = 'core';
    }

    // If submodule.
    $arr_sub_modules = array();
    $module_path_explode = explode('/', $module_path);
    $total_module_folder = count($module_path_explode);
    $arr_module_type_check = array('contrib', 'custom', 'features', 'core');
    for ($path_element = 0; $path_element < $total_module_folder - 1; $path_element++) {

      $mod_path_check = '';
      $module_check = $module_path_explode[$path_element];
      if ($module_check == $module) {
        continue;
      }
      for ($path_attach = 0; $path_attach <= $path_element; $path_attach++) {
        $mod_path_check .= $module_path_explode[$path_attach] . '/';
      }
      $mod_path_check .= $module_check . '.module';
      if (file_exists($mod_path_check)) {

        foreach ($arr_module_type_check as $module_type_check) {
          if (isset($arr_site_report['modules'][$module_type_check][$module_check])) {
            $arr_site_report['modules'][$module_type_check][$module_check]['sub_modules'][] = $module;
            $module_type = 'sub_module';
            break;
          }
        }
        if ($module_type != 'sub_module') {
          $arr_sub_modules[] = $module;
          $module = $module_check;
          $module_path = $mod_path_check;
        }
        break;
      }
    }

    if ($module_type == 'sub_module') {
      continue;
    }

    $module_exist_flag = FALSE;
    foreach ($arr_module_type_check as $module_type_check) {
      if (isset($arr_site_report['modules'][$module_type_check][$module]['sub_modules'])) {
        // $arr_sub_modules = $arr_site_report['modules'][$module_type_check][$module]['sub_modules'];
        $module_exist_flag = TRUE;
        break;
      }
    }

    if ($module_exist_flag) {
      continue;
    }

    $tmp_ready = [
      'd8available_flag' => 'N',
      'module_name' => $module,
      'd7_version' => $info['version'],
      'package' => $info['package'],
      'description' => $info['name'],
      'module_type' => $module_type,
      'd8_version' => '',
      'release' => '',
      'd8note' => '',
      'sub_modules' => $arr_sub_modules,
    ];

    if ($module_type == 'custom') {
      $arr_file_detail = get_custom_module_detail($module, $module_path);
      if (!empty($arr_file_detail['description'])) {
        $tmp_ready['description'] = $arr_file_detail['description'];
      }
      $tmp_ready['code_lines_fun_detail'] = $arr_file_detail;
      $arr_site_report['modules']['custom']['lines_of_code'][] = $tmp_ready['code_lines_fun_detail']['lines_count'];
      $arr_site_report['modules'][$module_type][$module] = $tmp_ready;
      continue;
    }

    if ($module_type == 'core') {
      $tmp_ready['d8available_flag'] = 'Y';
      $tmp_ready['d8_version'] = 'In core';
      $tmp_ready['enable_status'] = $info['status'];
      $arr_site_report['modules'][$module_type][$module] = $tmp_ready;
      if ($info['status'] == 'Enabled') {
        $arr_site_report['core_enabled'][] = $module;
      }
      continue;
    }

    // Check in d8 core.
    $project = drush_migration_audit_check_in_core($module);
    if (isset($project[$module]['in_core_since'])) {
      $tmp_ready['d8available_flag'] = 'Y';
      $tmp_ready['d8_version'] = 'In core since ' . $project[$module]['in_core_since'];
      $tmp_ready['d8note'] = $project[$module]['in_core_note'];
      // $arr_site_report['modules'][$module_type][$module] = $tmp_ready;
    }

    $url = "https://updates.drupal.org/release-history/" . $module . "/8.x";
    $xmlstr = migration_audit_get_xml_from_url($url);
    if ($xmlstr) {
      $xmlobj = new SimpleXMLElement($xmlstr);
      // For ease.
      $json = json_encode($xmlobj);
      $xml = json_decode($json, TRUE);
    }
    else {
      $xml = array();
    }
    // D8 version available.
    if (isset($xml['releases'])) {
      if (!empty($xml['releases'])) {
        // Fix weird XML issue.
        $release = (!empty($xml['releases']['release'][0])) ? $xml['releases']['release'][0] : $xml['releases']['release'];

        $tmp_ready['d8_version'] .= $tmp_ready['d8available_flag'] == 'Y' ? ', ' . $release['version'] : $release['version'];
        $tmp_ready['release'] = ($extra = $release['version_extra']) ? $extra : '';
        $tmp_ready['d8available_flag'] = 'Y';
      }

      if ($module_type == 'undefined') {
        $tmp_ready['module_type'] = 'contrib';
        $module_type = 'contrib';
      }
    }
    else {
      if (strpos($xml[0], '8.x') === FALSE) {
        $tmp_ready['module_type'] = 'custom';
        $module_type = 'custom';
        $arr_file_detail = get_custom_module_detail($module, $module_path);
        if (!empty($arr_file_detail['description'])) {
          $tmp_ready['description'] = $arr_file_detail['description'];
        }
        $tmp_ready['code_lines_fun_detail'] = $arr_file_detail;
        $arr_site_report['modules']['custom']['lines_of_code'][] = $tmp_ready['code_lines_fun_detail']['lines_count'];
      }
      else {
        $tmp_ready['module_type'] = 'contrib';
        $module_type = 'contrib';
      }
    }
    $arr_site_report['modules'][$module_type][$module] = $tmp_ready;
  }
  // Crone jobe detail.
  $arr_site_report['crons_using_modules'] = module_implements("cron");

  migration_audit_specified_module_existing_check($arr_site_report);
  migration_audit_payment_related_module_existing_check($arr_site_report);
  return $arr_site_report;
}

/**
 * Custom module details function.
 */
function get_custom_module_detail($module, $module_path) {

  $custom_module_path = str_replace($module . '.module', '', $module_path);
  $file_path = migration_audit_get_dir_contents($custom_module_path);
  $arr_check_extention = array('php', 'module', 'inc');
  $arr_file_detail = array('lines_count' => 0);
  foreach ($file_path as $file) {
    $path_split = explode('/', $file);
    $extention = explode('.', $path_split[count($path_split) - 1]);
    $extention = $extention[count($extention) - 1];
    if (in_array(strtolower($extention), $arr_check_extention)) {
      if (file_exists($file)) {
        $content_file = file_get_contents($file);
        $arr_file_detail['lines_count'] += substr_count($content_file, "\n");
        $re = '/function ([a-zA-Z_{1}][a-zA-Z0-9_]+)(?=\()/m';
        preg_match_all($re, $content_file, $matches, PREG_SET_ORDER, 0);
        foreach ($matches as $fun) {
          $arr_file_detail['file_function'][] = $fun[1];
        }
      }

    }
    elseif (strtolower($extention) == 'info') {
      if (file_exists($file)) {
        $content_info_file = file_get_contents($file);
        $arr_info = explode("\n", $content_info_file);
        foreach ($arr_info as $info_line) {

          if (strpos(strtolower($info_line), 'description') !== FALSE) {
            $arr_file_detail['description'] = trim(explode('=', $info_line)[1]);
          }
        }
      }
    }

  }
  return $arr_file_detail;
}

/**
 * Get XML from URL.
 */
function migration_audit_get_xml_from_url($url) {
  $ch = curl_init();

  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

  $xmlstr = curl_exec($ch);
  curl_close($ch);

  return $xmlstr;
}

/**
 * Specified check of existing modules.
 */
function migration_audit_specified_module_existing_check(&$smr_site_report) {
  $arr_check_module = array(
    'css_injector',
    'views_bulk_operations',
    'aet',
    'token',
    'ds',
    'rules',
    'php',
    'feeds',
    'job_scheduler',
    'js_injector',
    'markup',
    'panels',
    'panelizer',
    'rest_server',
    'restful',
    'ultimate_cron',
    'workbench',
    'workflow',
  );

  if (is_array($smr_site_report['modules']['contrib'])) {
    $result = array_intersect($arr_check_module, array_keys($smr_site_report['modules']['contrib']));
    $smr_site_report['specified_module_check'] = $result;
  }

}

/**
 * Payment related modules audit.
 */
function migration_audit_payment_related_module_existing_check(&$smr_site_report) {
  $arr_check_contrib_module = array(
    'payment',
  );
  $arr_check_custom_module = array(
    'scs_payment',
  );
  $result_contrib = array();
  $result_custom = array();

  if (is_array($smr_site_report['modules']['contrib'])) {
    $result_contrib = array_intersect($arr_check_contrib_module, array_keys($smr_site_report['modules']['contrib']));
  }
  else {
    $smr_site_report['modules']['contrib'] = [];
  }

  if (is_array($smr_site_report['modules']['custom'])) {
    $result_custom = array_intersect($arr_check_custom_module, array_keys($smr_site_report['modules']['custom']));
  }
  else {
    $smr_site_report['modules']['custom'] = [];
  }
  $smr_site_report['payment_related_module_check'] = array_merge($result_contrib, $result_custom);

}

/**
 * Callback for the content-type-list command.
 */
function migration_audit_drush_content_type_list() {
  $content_types = node_type_get_types();
  return $content_types;
}

/**
 * Core informations.
 */
function drush_migration_audit_check_in_core($project) {
  switch ($project) {

    case 'admin_language':
    case 'entity_translation':
    case 'fallback_language_negotiation':
    case 'i18n':
    case 'i18nviews':
    case 'l10n_install':
    case 'l10n_update':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_complete'] = TRUE;
      $projects[$project]['in_core_note'] = t('Replaced by core localization functionality, the core Language module, and the core Configuration, Content, and Interface Translation modules.');
      break;

    case 'admin_views':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_note'] = t('Integrated with the core Views module. No comment admin view in 8.0.x.');
      break;

    case 'bean':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_complete'] = TRUE;
      $projects[$project]['in_core_note'] = t('Replaced by the core Custom Block module.');
      break;

    case 'breakpoint':
    case 'breakpoints':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_complete'] = TRUE;
      break;

    case 'cachetags':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_complete'] = TRUE;
      $projects[$project]['in_core_note'] = t('Replaced by core APIs.');
      break;

    case 'caption_filter':
    case 'float_filter':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_complete'] = TRUE;
      $projects[$project]['in_core_note'] = t('Replaced by functionality in the core Editor module.');
      break;

    case 'ckeditor':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_complete'] = TRUE;
      break;

    case 'ctools':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_note'] = t('Mostly replaced by core APIs, including modal dialogs, exportables, and plugins. Excludes <a href="@url">Page Manager</a> and Form Wizard.', array(
        '@url' => 'https://www.drupal.org/project/page_manager',
      ));
      break;

    case 'date':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_note'] = t('No recurring dates support. See <a href="@extras">Datetime Extras: Provide a field for repeating / recuring dates</a> and <a href="@field">Recurring Dates Field</a>', array(
        '@extras' => 'https://www.drupal.org/project/datetime_extras/issues/2775249',
        '@field' => 'https://www.drupal.org/project/date_recur',
      ));
      break;

    case 'date_popup_authored':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_complete'] = TRUE;
      break;

    case 'edit':
    case 'quickedit':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_complete'] = TRUE;
      break;

    case 'email':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_note'] = t('E-mail address contact forms are not supported by core.');
      break;

    case 'entityreference':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_complete'] = TRUE;
      break;

    case 'entity':
    case 'entity_view_mode':
    case 'file_entity':
    case 'title':
    case 'user_picture_field':
    case 'uuid':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_complete'] = TRUE;
      $projects[$project]['in_core_note'] = t('Replaced by core Entity system functionality.');
      break;

    case 'features':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_note'] = t('The original intended functionality of the Features module is not provided by core, but the core Configuration system provides support for importing, exporting, and overriding site configuration.');
      break;

    case 'field_extrawidgets':
    case 'hidden_field':
    case 'field_hidden':
    case 'hidden_widget':
    case 'formfilter':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_note'] = t('Fields can be hidden natively from the Form Display configuration. To make fields read-only, use the <a href="@url">Read-only Field Widget</a> module.', array(
        '@url' => 'https://www.drupal.org/project/readonly_field_widget',
      ));
      break;

    case 'field_formatter_settings':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_complete'] = TRUE;
      break;

    case 'fieldable_panels_panes':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_note'] = t('Custom block types provide all of the functionality that is necessary.');
      $projects[$project]['in_core_complete'] = TRUE;
      break;

    case 'link':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_note'] = t('No support for internal links.');
      break;

    case 'migrate':
    case 'migrate_d2d':
    case 'migrate_drupal':
      $projects[$project]['in_core_since'] = '8.x';
      break;

    case 'module_filter':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_note'] = t('A search functionality is included on the core modules page. The re-designed modules page in the 2.x branch is not in core.');
      break;

    case 'navbar':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_complete'] = TRUE;
      $projects[$project]['in_core_note'] = t('Replaced by the updated core Toolbar module.');
      break;

    case 'options_element':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_complete'] = TRUE;
      $projects[$project]['in_core_note'] = t('Taxonomy Term Reference fields have much better usability with fewer drawbacks.');
      break;

    case 'panels':
    case 'panelizer':
      $projects[$project]['in_core_since'] = '8.5';
      $projects[$project]['in_core_complete'] = TRUE;
      $projects[$project]['in_core_note'] = t('Use the <a href="@url">Layout Builder</a> module.', array(
        '@url' => 'https://www.drupal.org/node/2924128',
      ));
      break;

    case 'phone':
    case 'telephone':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_complete'] = TRUE;
      break;

    case 'picture':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_complete'] = TRUE;
      $projects[$project]['in_core_note'] = t('Replaced by the core Responsive Image module.');
      break;

    case 'elements':
    case 'placeholder':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_complete'] = TRUE;
      $projects[$project]['in_core_note'] = t('Replaced by HTML5 form API functionality.');
      break;

    case 'references':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_complete'] = TRUE;
      break;

    case 'restws':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_complete'] = TRUE;
      $projects[$project]['in_core_note'] = t('Replaced by the core REST module.');
      break;

    case 'schemaorg':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_note'] = t('The default RDF mappings of Drupal core have been updated to include schema.org in Drupal 8. Also, a lot of the backend code of this module was ported into Drupal 8 core. The user interface that allows one to set the mappings now lives in the <a href="@url">RDF UI</a> module.', array(
        '@url' => 'https://www.drupal.org/project/rdfui',
      ));
      break;

    case 'services':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_note'] = t('The core REST module provides most of the functionality from previous versions of the Services module.');
      break;

    case 'stringoverrides':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_note'] = t('The core Interface Translation module allows custom translations to be provided for strings in any language, including English.');
      break;

    case 'transliteration':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_note'] = t('Replaced by core APIs. No direct support for transliterating path aliases or file names.');
      break;

    case 'variable':
    case 'defaultconfig':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_complete'] = TRUE;
      $projects[$project]['in_core_note'] = t('Replaced by the core Configuration system.');
      break;

    case 'views':
    case 'extra_columns':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_complete'] = TRUE;
      break;

    case 'views_bulk_operations':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_note'] = t('The core Views module provides bulk operations on simple actions only. No support for batch operations or configurable actions.');
      break;

    case 'views_datasource':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_note'] = t('The basic functionality is in core, but some advanced features (such as outputting a Views attachment as JSON) are not.');
      break;

    case 'views_between_dates_filter':
      $projects[$project]['in_core_since'] = '8.6';
      $projects[$project]['in_core_note'] = t('While <a href="@change_record">Views integration for the Datetime Range module</a> is now in core, <a href="@granularity">Views Date Filter Datetime Granularity Option</a> is still missing, but will hopefully land in core soon.', array(
        '@change_record' => 'https://www.drupal.org/node/2857691',
        '@granularity' => 'https://www.drupal.org/project/drupal/issues/2868014',
      ));
      break;

    case 'views_filters_populate':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_complete'] = TRUE;
      $projects[$project]['in_core_note'] = t('When adding a filter, select Combine Fields Filter from the Global category.');
      break;

    case 'views_responsive_grid':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_complete'] = TRUE;
      $projects[$project]['in_core_note'] = t('This module will not be ported for Drupal 8. Views grids in core have been replaced with DIVs.');
      break;

    case 'wysiwyg':
      $projects[$project]['in_core_since'] = '8.x';
      $projects[$project]['in_core_note'] = t('API support added to the core "Editor" module. No support for multiple text editors per text format.');
      break;

    // Also correct information about D7 modules.
    case 'cck':
      $projects[$project]['in_core_note'] = '';
      $projects[$project]['in_core_complete'] = TRUE;
      break;

    default:
      // Any other module is not included in core.
      $core = FALSE;
  }
  return $projects;
}

/**
 * Contents audit.
 */
function migration_audit_get_dir_contents($dir, &$results = array()) {
  $files = scandir($dir);

  foreach ($files as $key => $value) {
    $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
    if (!is_dir($path)) {
      $results[] = $path;
    }
    elseif ($value != "." && $value != "..") {
      migration_audit_get_dir_contents($path, $results);
      // $results[] = $path;
    }
  }

  return $results;
}

/**
 * Creates a markdown document based on the parsed documentation.
 *
 * @author Peter-Christoph Haider <peter.haider@zeyon.net>
 * @package Apidoc
 * @version 1.00 (2014-04-04)
 * @license GNU Lesser Public License
 */
class TextTable {
  /**
   * Generate the table format in md file.
   *
   * @var intThesourcepath*/
  public $maxlen = 255;
  /**
   * Source path.
   *
   * @var arrayThesourcepath*/
  private $data = array();
  /**
   * Source path.
   *
   * @var arrayThesourcepath*/
  private $header = array();
  /**
   * Source path.
   *
   * @var arrayThesourcepath*/
  private $len = array();
  /**
   * Source path.
   *
   * @var arrayThesourcepath*/
  private $align = array(
    'name' => 'L',
    'type' => 'C',
  );

  /**
   * Method construct.
   *
   * @param array $header
   *   The header array [key => label, ...].
   * @param array $content
   *   Content.
   * @param array $align
   *   Alignment optios [key => L|R|C, ...].
   */
  public function __construct($header = NULL, $content = array(), $align = FALSE) {
    if ($header) {
      $this->header = $header;
    }
    elseif ($content) {
      foreach ($content[0] as $key => $value) {
        $this->header[$key] = $key;
      }
    }

    foreach ($this->header as $key => $label) {
      $this->len[$key] = strlen($label);
    }

    if (is_array($align)) {
      $this->setAlgin($align);
    }

    $this->addData($content);
  }

  /**
   * Overwrite the alignment array.
   *
   * @param array $align
   *   Alignment optios [key => L|R|C, ...].
   */
  public function setAlgin($align) {
    $this->align = $align;
  }

  /**
   * Add data to the table.
   *
   * @param array $content
   *   Content.
   */
  public function addData($content) {
    foreach ($content as &$row) {
      foreach ($this->header as $key => $value) {
        if (!isset($row[$key])) {
          $row[$key] = '-';
        }
        elseif (strlen($row[$key]) > $this->maxlen) {
          $this->len[$key] = $this->maxlen;
          $row[$key] = substr($row[$key], 0, $this->maxlen - 3) . '...';
        }
        elseif (strlen($row[$key]) > $this->len[$key]) {
          $this->len[$key] = strlen($row[$key]);
        }
      }
    }

    $this->data = $this->data + $content;
    return $this;
  }

  /**
   * Add a delimiter.
   *
   * @return string
   *   Return the delimiter.
   */
  private function renderDelimiter() {
    $res = '|';
    foreach ($this->len as $key => $l) {
      $res .= (isset($this->align[$key]) && ($this->align[$key] == 'C' || $this->align[$key] == 'L') ? ':' : ' ')
                    . str_repeat('-', $l)
                    . (isset($this->align[$key]) && ($this->align[$key] == 'C' || $this->align[$key] == 'R') ? ':' : ' ')
                    . '|';
    }
    return $res . "\n";
  }

  /**
   * Render a single row.
   *
   * @param array $row
   *   Table row.
   *
   * @return string
   *   Return row of table.
   */
  private function renderRow($row) {
    $res = '|';
    foreach ($this->len as $key => $l) {
      $res .= ' ' . $row[$key] . ($l > strlen($row[$key]) ? str_repeat(' ', $l - strlen($row[$key])) : '') . ' |';
    }

    return $res . "\n";
  }

  /**
   * Render the table.
   *
   * @param array $content
   *   Additional table content.
   *
   * @return string
   *   Return the string
   */
  public function render($content = array()) {

    if (!empty($content)) {
      $this->addData($content);
    }
    $res = $this->renderRow($this->header)
               . $this->renderDelimiter();
    foreach ($this->data as $row) {
      $res .= $this->renderRow($row);
    }

    return $res;
  }

}
