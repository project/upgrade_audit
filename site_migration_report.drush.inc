<?php

/**
 * @file
 * The drush commands.
 */

require 'site_migration_report_support_fun.inc';
require 'site_migration_report_elements.inc';

/**
 * Implements hook_drush_command().
 */
function site_migration_report_drush_command() {
  $items['site_migration_report'] = array(
    'description' => 'Show a migration audit report.',
    'aliases' => array('smr'),
    'arguments' => array(
      'site_name' => '(Optional) sitename.',
      'format' => '(Optional) format.',
    ),
  );
  return $items;
}

/**
 * Drush command callback for Audit.
 */
function drush_site_migration_report($format = 'json', $site_name = 'undefined') {
  $description_value = smr_upgrade_audit_description();
  $arr_content_index = smr_index_array($description_value);

  $arr_report = array();
  smr_call_elements_function($arr_content_index, $arr_report, $format);
  smr_render_report($arr_report, $format);
}

/**
 * Callback for Audit batch UI.
 */
function upgrade_audit_ui_generate_report($format = 'json', $function, &$context) {
  $path = variable_get('file_private_path');
  if (empty($path)) {
    $path = variable_get('file_public_path', 'sites/default/files');
  }
  if (!file_exists(DRUPAL_ROOT . '/' . $path . '/audit-report')) {
    mkdir(DRUPAL_ROOT . '/' . $path . '/audit-report');
  }
  $filename = DRUPAL_ROOT . '/' . $path . '/audit-report/upgrade-audit-report.md';
  $function_name = $function[0];
  $command = 'drush php-eval "upgrade_audit_ui_generate_section(' . $function_name . ')" >> ' . $filename . ' ';
  shell_exec($command);
  $context['message'] = 'Get Details of ' . $function[1];
}

/**
 * Generate each section for Audit batch UI.
 */
function upgrade_audit_ui_generate_section($function_name) {
  $description_value = smr_upgrade_audit_description();
  $arr_content_index = smr_index_array($description_value);
  $arr_report = array();
  $function_data = array($function_name => $arr_content_index[$function_name]);
  smr_call_elements_function($function_data, $arr_report, 'md');
  $format = 'md';
  smr_render_report($arr_report, $format);
}

/**
 * Call all the element functions.
 */
function smr_call_elements_function($arr_content_index, &$arr_report, $format, $stage = 0) {
  foreach ($arr_content_index as $function_name => $data) {
    if (!empty($data['wrapper'])) {
      $arr_report[] = array(
        array(
          'template_type' => 'wrapper_start',
          'class' => $data['wrapper'],
        ),
      );
    }
    $arr_report_description = array();
    $arr_report_description = array(
      array(
        'template_type' => 'heading',
        'stage' => $stage,
        'data' => $data['title'],
        'id' => $function_name,
      ),
      array(
        'template_type' => 'description',
        'data' => $data['description'],
      ),
      array(
        'template_type' => 'what_means',
        'what_means' => $data['what_means'],
      ),
      array(
        'template_type' => 'read_more',
        'read_more' => $data['read_more'],
      ),
    );
    $arr_report[] = $arr_report_description;
    if (function_exists($function_name)) {
      $return_result = $function_name($arr_report);
      if ($return_result === FALSE) {
        $arr_report[] = array(
          array(
            'template_type' => 'paragraph',
            'data' => "No detail found.\n",
          ),
        );
      }
    }
    if (!empty($data['sub_elements'])) {
      smr_call_elements_function($data['sub_elements'], $arr_report, $format, ($stage + 1));
    }
    if (!empty($data['wrapper'])) {
      $arr_report[] = array(
        array(
          'template_type' => 'wrapper_end',
          'class' => $function_name,
        ),
      );
    }
  }
}

/**
 * Render the report.
 */
function smr_render_report($arr_report, $format = 'json') {

  foreach ($arr_report as $arr_report_element) {
    $template_fun_name = 'smr_' . $format . '_template';
    $template_fun_name($arr_report_element);
  }
}

/**
 * Create report in md format.
 */
function smr_md_template($arr_report_element) {
  foreach ($arr_report_element as $arr_report_element_data) {
    switch ($arr_report_element_data['template_type']) {

      case 'heading':
        $stage_heading = '';
        for ($counter = 0; $counter <= $arr_report_element_data['stage']; $counter++) {
          $stage_heading .= '#';
        }
        $stage_heading .= ' ' . $arr_report_element_data['data'] . '{#' . $arr_report_element_data['id'] . '}';
        drush_print($stage_heading);
        break;

      case 'description':
        $description = '######' . $arr_report_element_data['data'];
        drush_print($description);
        break;

      case 'what_means':
        if ($arr_report_element_data['what_means']) {
          drush_print('###### **What this means: **' . $arr_report_element_data['what_means']);
        }
        break;

      case 'read_more':
        if ($arr_report_element_data["read_more"]) {
          drush_print('###### [Read More](' . $arr_report_element_data["read_more"] . ')');
        }
        break;

      case 'list':
        break;

      case 'wrapper_start':
        $wrapper_text = "<div markdown='1' class='" . $arr_report_element_data['class'] . "'>";
        drush_print($wrapper_text);
        break;

      case 'wrapper_end':
        drush_print("</div>");
        break;

      case 'line_with_value':

        if (empty($arr_report_element_data['value'])) {
          $arr_report_element_data['value'] = '-';
        }
        drush_print("```");
        drush_print($arr_report_element_data['label'] . ": " . $arr_report_element_data['value']);
        drush_print("```");
        break;

      case 'line_with_value_list':
        drush_print("```");
        foreach ($arr_report_element_data['data'] as $row_line) {
          if (empty($row_line['value'])) {
            $row_line['value'] = '-';
          }
          drush_print($row_line['label'] . ": " . $row_line['value']);
        }
        drush_print("```");
        break;

      case 'summary_list':
        drush_print("```");
        foreach ($arr_report_element_data['data'] as $row_line) {
          if (empty($row_line['value'])) {
            $row_line['value'] = '-';
          }
          drush_print($row_line['label'] . ": " . $row_line['value']);
        }
        drush_print("```");
        break;

      case 'paragraph':
        drush_print($arr_report_element_data['data']);
        break;

      case 'table':
        $table = new TextTable($arr_report_element_data['row_head'], $arr_report_element_data['data']);
        drush_print($table->render());
        drush_print("```");
        if ($arr_report_element_data['lines_of_code']) {
          drush_print("Total lines of custom code: " . ($arr_report_element_data['lines_of_code']));
        }
        else {
          drush_print("Total records: " . count($arr_report_element_data['data']));
        }
        drush_print("```");
        break;
    }
  }

}

/**
 * Create report in json format.
 */
function smr_json_template($arr_report_element) {
  $arr_site_report = smr_get_general_site_detail();
  $json_site_audit_report = json_encode($arr_site_report);
  drush_print($json_site_audit_report);
}

/**
 * Create description and read more link for each sections.
 */
function smr_upgrade_audit_description() {
  $url = 'https://d7upgrade.org/data/description/descriptiontext.json';
  $data = upgrade_audit_validate_url($url);
  $path = variable_get('file_private_path');
  if (empty($path)) {
    $path = variable_get('file_public_path', 'sites/default/files');
  }
  $file_path = DRUPAL_ROOT . '/' . $path . '/audit-report/descriptiontext.json';
  if ($data == '404') {
    if (file_exists($file_path)) {
      $data = file_get_contents($file_path);
      if (empty($data)) {
        $data = [];
      }
    }
    else {
      $data = [];
    }
  }
  else {
    if (file_exists($file_path)) {
      unlink($file_path);
    }
    file_put_contents($file_path, $data);
  }
  $array_data = json_decode($data);
  foreach($array_data as $key=>$value) {
    $description[$value->title] = array(
      'what' => $value->field_what,
      'what_means' => $value->field_what_it_means,
      'readmore' => $value->field_read_more_link,
    );
  }
  return $description;
}
