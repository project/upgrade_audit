/**
 * @file Custom JS changes for Audit report print
 */

 (function ($) {

  /**
   * Custom behavior for Upgrade Audit.
   *
   * Export the Audit Report in a pdf form and give option to print
   */
  Drupal.behaviors.upgradeAudit = {
    attach: function (context, settings) {
      $(document).ready(function () {
        //sticky sidebar
        var wrapper = $('.report-main-wrapper');
        var main = $('.report-content-main');
        var sidebar = $('.stickthis #edit-index');
        var sidebarWrapper = $('.stickthis');

        $(window).scroll(function () {
          sidebar.css({'display':'block', 'width': sidebarWrapper.width() + 'px'});

          if(sidebar.outerHeight() < main.outerHeight()){
            if(sidebar.outerHeight() > $(window).innerHeight()) {
              if ((wrapper[0].getBoundingClientRect().bottom < ($(window).innerHeight()))
                  && ((wrapper[0].getBoundingClientRect().bottom < sidebar.outerHeight()))
                ) {
                wrapper.removeClass("fix-top-VP");
                wrapper.addClass("flex-bottom");
              }
              else if (wrapper[0].getBoundingClientRect().top < 0 ) {
                wrapper.addClass("fix-top-VP");
                wrapper.removeClass("flex-bottom");
              }
              else {
                wrapper.removeClass("fix-top-VP");
                wrapper.removeClass("flex-bottom");
              }
            } else if (sidebar.outerHeight() < wrapper.outerHeight()) {
              if (wrapper[0].getBoundingClientRect().bottom < ($(window).innerHeight())) {
                wrapper.removeClass("fix-bottom-VP");
                wrapper.addClass("flex-bottom");
              }
              else if (wrapper[0].getBoundingClientRect().bottom > (sidebar.outerHeight() + $(window).innerHeight())) {
                wrapper.removeClass("fix-bottom-VP");
                wrapper.removeClass("flex-bottom");
              }
              else {
                wrapper.addClass("fix-bottom-VP");
                wrapper.removeClass("flex-bottom");
              }
            }
          }
        });
      });
      var acc = document.getElementsByClassName("accordion");
      var i;
      for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
          this.classList.toggle("active");
          var panel = this.nextElementSibling;
          if (panel.style.display === "block") {
            panel.style.display = "none";
          } else {
            panel.style.display = "block";
          }
        });
      }
    }
  }
})(jQuery);
