<?php

/**
 * @file
 * Include extra file for elements call.
 */

/**
 * Report index.
 */
function smr_index_array($description_value) {
  $arr_content_index = array(
    'smr_summary' => array(
      'title' => 'Summary',
      'description' => $description_value['Summary']['what'],
      'what_means' => $description_value['Summary']['what_means'],
      'read_more' => $description_value['Summary']['readmore'],
      'sub_elements' => array(),
      'wrapper' => 'summary-wrapper',
    ),
    'smr_basic_information' => array(
      'title' => 'Basic information',
      'description' => $description_value['Basic information']['what'],
      'what_means' => $description_value['Basic information']['what_means'],
      'read_more' => $description_value['Basic information']['readmore'],
      'sub_elements' => array(),
    ),
    'smr_content' => array(
      'title' => 'Content',
      'description' => $description_value['Content']['what'],
      'what_means' => $description_value['Content']['what_means'],
      'read_more' => $description_value['Content']['readmore'],
      'sub_elements' => array(
        'smr_content_types' => array(
          'title' => 'Content types',
          'description' => $description_value['Content types']['what'],
          'what_means' => $description_value['Content types']['what_means'],
          'read_more' => $description_value['Content types']['readmore'],
          'sub_elements' => array(),
        ),
        'smr_nodes' => array(
          'title' => 'Nodes',
          'description' => $description_value['Nodes']['what'],
          'what_means' => $description_value['Nodes']['what_means'],
          'read_more' => $description_value['Nodes']['readmore'],
          'sub_elements' => array(),
        ),
        'smr_files' => array(
          'title' => 'Files',
          'description' => $description_value['Files']['what'],
          'what_means' => $description_value['Files']['what_means'],
          'read_more' => $description_value['Files']['readmore'],
          'sub_elements' => array(),
        ),
        'smr_webforms' => array(
          'title' => 'Webforms',
          'description' => $description_value['Webforms']['what'],
          'what_means' => $description_value['Webforms']['what_means'],
          'read_more' => $description_value['Webforms']['readmore'],
          'sub_elements' => array(),
        ),
        'smr_taxonomy' => array(
          'title' => 'Taxonomy',
          'description' => $description_value['Taxonomy']['what'],
          'what_means' => $description_value['Taxonomy']['what_means'],
          'read_more' => $description_value['Taxonomy']['readmore'],
          'sub_elements' => array(),
        ),
        'smr_views' => array(
          'title' => 'Views',
          'description' => $description_value['Views']['what'],
          'what_means' => $description_value['Views']['what_means'],
          'read_more' => $description_value['Views']['readmore'],
          'sub_elements' => array(),
        ),
        'smr_blocks' => array(
          'title' => 'Blocks',
          'description' => $description_value['Blocks']['what'],
          'what_means' => $description_value['Blocks']['what_means'],
          'read_more' => $description_value['Blocks']['readmore'],
          'sub_elements' => array(
            'smr_list_of_enabled_blocks' => array(
              'title' => 'List of enabled blocks',
              'description' => $description_value['List of enabled blocks']['what'],
              'what_means' => $description_value['List of enabled blocks']['what_means'],
              'read_more' => $description_value['List of enabled blocks']['readmore'],
              'sub_elements' => array(),
            ),
            'smr_list_of_enabled_region' => array(
              'title' => 'List of enabled regions',
              'description' => $description_value['List of enabled regions']['what'],
              'what_means' => $description_value['List of enabled regions']['what_means'],
              'read_more' => $description_value['List of enabled regions']['readmore'],
              'sub_elements' => array(),
            ),
          ),
        ),
      ),
    ),
    'smr_modules' => array(
      'title' => 'Modules',
      'description' => $description_value['Modules']['what'],
      'what_means' => $description_value['Modules']['what_means'],
      'read_more' => $description_value['Modules']['readmore'],
      'sub_elements' => array(
        'smr_contributed_modules' => array(
          'title' => 'Contributed Modules',
          'description' => $description_value['Contributed Modules']['what'],
          'what_means' => $description_value['Contributed Modules']['what_means'],
          'read_more' => $description_value['Contributed Modules']['readmore'],
          'sub_elements' => array(),
        ),
        'smr_custom_modules' => array(
          'title' => 'Custom Modules',
          'description' => $description_value['Custom Modules']['what'],
          'what_means' => $description_value['Custom Modules']['what_means'],
          'read_more' => $description_value['Custom Modules']['readmore'],
          'sub_elements' => array(),
        ),
        'smr_core_modules' => array(
          'title' => 'Core Modules',
          'description' => $description_value['Core Modules']['what'],
          'what_means' => $description_value['Core Modules']['what_means'],
          'read_more' => $description_value['Core Modules']['readmore'],
          'sub_elements' => array(),
        ),
        'smr_features' => array(
          'title' => 'Features',
          'description' => $description_value['Features']['what'],
          'what_means' => $description_value['Features']['what_means'],
          'read_more' => $description_value['Features']['readmore'],
          'sub_elements' => array(),
        ),
        'smr_additional_information' => array(
          'title' => 'Additional information',
          'description' => $description_value['Additional information']['what'],
          'what_means' => $description_value['Additional information']['what_means'],
          'read_more' => $description_value['Additional information']['readmore'],
          'sub_elements' => array(),
        ),
      ),
    ),
    'smr_theme' => array(
      'title' => 'Theme',
      'description' => $description_value['Theme']['what'],
      'what_means' => $description_value['Theme']['what_means'],
      'read_more' => $description_value['Theme']['readmore'],
      'sub_elements' => array(
        'smr_list_of_templates_in_the_custom_theme' => array(
          'title' => 'List of templates in the custom theme',
          'description' => $description_value['List of templates in the custom theme']['what'],
          'what_means' => $description_value['List of templates in the custom theme']['what_means'],
          'read_more' => $description_value['List of templates in the custom theme']['readmore'],
          'sub_elements' => array(),
        ),
        'smr_list_of_js_files_included' => array(
          'title' => 'List of JS files included',
          'description' => $description_value['List of JS files included']['what'],
          'what_means' => $description_value['List of JS files included']['what_means'],
          'read_more' => $description_value['List of JS files included']['readmore'],
          'sub_elements' => array(),
        ),
        'smr_list_of_css_files_included' => array(
          'title' => 'List of CSS files includeds',
          'description' => $description_value['List of CSS files includeds']['what'],
          'what_means' => $description_value['List of CSS files includeds']['what_means'],
          'read_more' => $description_value['List of CSS files includeds']['readmore'],
          'sub_elements' => array(),
        ),
        'smr_templates_php' => array(
          'title' => 'Templates.php',
          'description' => $description_value['Templates.php']['what'],
          'what_means' => $description_value['Templates.php']['what_means'],
          'read_more' => $description_value['Templates.php']['readmore'],
          'sub_elements' => array(),
        ),
        'smr_list_of_functions_used' => array(
          'title' => 'List of functions used',
          'description' => $description_value['List of functions used']['what'],
          'what_means' => $description_value['List of functions used']['what_means'],
          'read_more' => $description_value['List of functions used']['readmore'],
          'sub_elements' => array(),
        ),
      ),
    ),
    'smr_users' => array(
      'title' => 'Users',
      'description' => $description_value['Users']['what'],
      'what_means' => $description_value['Users']['what_means'],
      'read_more' => $description_value['Users']['readmore'],
      'sub_elements' => array(),
    ),
    'smr_crons' => array(
      'title' => 'Crons',
      'description' => $description_value['Crons']['what'],
      'what_means' => $description_value['Crons']['what_means'],
      'read_more' => $description_value['Crons']['readmore'],
      'sub_elements' => array(),
    ),
  );
  return $arr_content_index;
}

/**
 * Functopn for generating report element.
 */
function smr_content_types(&$arr_report) {

  $arr_site_content_type_report = smr_get_general_site_detail();
  if (empty($arr_site_content_type_report['content_types'])) {
    return FALSE;
  }
  $arr_col = ['Machine name', 'Name', 'Number of fields', 'Description'];
  $arr_content_type = array();
  foreach ($arr_site_content_type_report['content_types'] as $mechine_name => $content_type) {
    $arr_content_type[] = array(
      $mechine_name,
      $content_type['name'],
      count($arr_site_content_type_report['content_types_fields'][$mechine_name]) + 1,
      $content_type['description'],
    );
  }

  $arr_report[] = array(
        array(
          'template_type' => 'table',
          'row_head' => $arr_col,
          'data' => $arr_content_type,
        ),
  );
}

/**
 * Functopn for generating report element.
 */
function smr_nodes(&$arr_report) {

  $arr_site_content_type_report = smr_get_general_site_detail();
  if (empty($arr_site_content_type_report['node_published'])) {
    return FALSE;
  }
  $arr_col = ['Content type', 'published node'];
  $arr_contenttype_node = array();
  foreach ($arr_site_content_type_report['node_published'] as $key => $val) {
    $arr_contenttype_node[] = array($key, $val);
  }

  $arr_report[] = array(
        array(
          'template_type' => 'table',
          'row_head' => $arr_col,
          'data' => $arr_contenttype_node,
        ),
  );
}

/**
 * Functopn for generating report element.
 */
function smr_files(&$arr_report) {

  // Upload file information.
  $totalFileCount = db_query("SELECT COUNT(fid) AS totalFiles FROM {file_managed} WHERE status = 1")->fetchObject()->totalFiles;

  $arr_report[] = array(
        array(
          'template_type' => 'line_with_value',
          'label' => 'Number of Images and files in drupal file entity table',
          'value' => $totalFileCount,
        ),
  );
}

/**
 * Functopn for generating report element.
 */
function smr_webforms(&$arr_report) {

  $arr_site_content_type_report = smr_get_general_site_detail();
  if (empty($arr_site_content_type_report['webform'])) {
    return FALSE;
  }
  $arr_col = ['Webform Name', 'Toatal Submission'];
  $arr_webform = array();
  foreach ($arr_site_content_type_report['webform'] as $key => $val) {
    $arr_webform[] = array($val['name'], $val['submission']);
  }

  $arr_report[] = array(
        array(
          'template_type' => 'table',
          'row_head' => $arr_col,
          'data' => $arr_webform,
        ),
  );
}

/**
 * FUnctopn for generating report element.
 */
function smr_taxonomy(&$arr_report) {

  $arr_site_report = smr_get_general_site_detail();
  if (empty($arr_site_report['taxonomies'])) {
    return FALSE;
  }
  $arr_col = ['Vocabularies', 'Number of terms'];
  $arr_taxonomy = array();
  foreach ($arr_site_report['taxonomies'] as $voc_id => $val) {
    $arr_taxonomy[] = array($voc_id, $val['total_term']);
  }

  $arr_report[] = array(
        array(
          'template_type' => 'table',
          'row_head' => $arr_col,
          'data' => $arr_taxonomy,
        ),
  );
}

/**
 * Functopn for generating report element.
 */
function smr_views(&$arr_report) {

  $arr_site_report = smr_get_general_site_detail();
  if (empty($arr_site_report['views'])) {
    return FALSE;
  }
  $arr_col = ['View', 'Number of display', 'description'];
  $arr_views = array();
  foreach ($arr_site_report['views'] as $view_name => $view_detail) {
    $arr_views[] = array(
      $view_name,
      count($view_detail['display']),
      $view_detail['description'],
    );
  }

  $arr_report[] = array(
        array(
          'template_type' => 'paragraph',
          'data' => "List of enabled views with number of displays and description:\n",
        ),
  );

  $arr_report[] = array(
        array(
          'template_type' => 'table',
          'row_head' => $arr_col,
          'data' => $arr_views,
        ),
  );
}

/**
 * Functopn for generating report element.
 */
function smr_list_of_enabled_blocks(&$arr_report) {

  $arr_site_report = smr_get_general_site_detail();
  if (empty($arr_site_report['theme']['default']['block'])) {
    return FALSE;
  }
  $arr_col = ['Blocks'];
  $row_table = array();
  foreach ($arr_site_report['theme']['default']['block'] as $key => $val) {
    $row_table[$key][] = $val;
  }

  $arr_report[] = array(
        array(
          'template_type' => 'paragraph',
          'data' => "List of enabled blocks:\n",
        ),
  );

  $arr_report[] = array(
        array(
          'template_type' => 'table',
          'row_head' => $arr_col,
          'data' => $row_table,
        ),
  );
}

/**
 * Functopn for generating report element.
 */
function smr_list_of_enabled_region(&$arr_report) {

  $arr_site_report = smr_get_general_site_detail();
  if (empty($arr_site_report['theme']['default']['regions'])) {
    return FALSE;
  }
  $arr_col = ['Regions'];
  $row_table = array();
  foreach ($arr_site_report['theme']['default']['regions'] as $key => $val) {
    $row_table[$key][] = $val;
  }

  $arr_report[] = array(
        array(
          'template_type' => 'paragraph',
          'data' => "List of enabled regions:\n",
        ),
  );

  $arr_report[] = array(
        array(
          'template_type' => 'table',
          'row_head' => $arr_col,
          'data' => $row_table,
        ),
  );
}

/**
 * Functopn for generating report element.
 */
function smr_contributed_modules(&$arr_report) {

  $arr_site_report = smr_get_general_site_detail();
  if (empty($arr_site_report['modules']['contrib'])) {
    return FALSE;
  }
  $arr_col = [
    'D9 Status(Y/N)',
    'Name', 'Package',
    'Current Version',
    'D9 Version',
    'Submodule',
    'note',
  ];

  $arr_mod_table = array();
  foreach ($arr_site_report['modules']['contrib'] as $module) {
    $arr_mod_table[] = array($module['d8available_flag'],
      $module['module_name'],
      $module['package'],
      $module['d7_version'],
      $module['d8_version'],
      implode(', ', $module['sub_modules']),
      $module['d8note'],
    );
  }

  $arr_report[] = array(
        array(
          'template_type' => 'paragraph',
          'data' => "List of contributed modules enabled with D8 status:\n",
        ),
  );

  $arr_report[] = array(
        array(
          'template_type' => 'table',
          'row_head' => $arr_col,
          'data' => $arr_mod_table,
        ),
  );
}

/**
 * Functopn for generating report element.
 */
function smr_custom_modules(&$arr_report) {

  $arr_site_report = smr_get_general_site_detail();
  if (empty($arr_site_report['modules']['custom'])) {
    return FALSE;
  }
  $arr_col = [
    'Module',
    'Line of code',
    'functions',
    'Description',
    'Submodules',
  ];

  $arr_mod_table = array();
  foreach ($arr_site_report['modules']['custom'] as $module) {
    if ($module['code_lines_fun_detail']['lines_count']) {
      $arr_mod_table[] = array(
        $module['module_name'],
        $module['code_lines_fun_detail']['lines_count'],
        implode(', ', $module['code_lines_fun_detail']['file_function']),
        $module['description'],
        implode(',', $module['sub_modules']),
      );
    }
    $lines_of_code[] = $module['code_lines_fun_detail']['lines_count'];
  }
  $total_lines_of_code = array_sum($lines_of_code);
  $flag = count($module_sp = $arr_site_report['payment_related_module_check']) > 0 ? 'Y' : 'N';
  $module_sp = $flag == 'Y' ? " (" . implode(', ', $module_sp) . ')' : '';

  $arr_report[] = array(
        array(
          'template_type' => 'paragraph',
          'data' => "List of custom modules enabled: \n",
        ),
  );

  $arr_report[] = array(
        array(
          'template_type' => 'table',
          'row_head' => $arr_col,
          'data' => $arr_mod_table,
          'lines_of_code' => $total_lines_of_code,
        ),
  );

  $arr_report[] = array(
        array(
          'template_type' => 'line_with_value',
          'label' => 'Payment related modules used',
          'value' => $flag . $module_sp,
        ),
  );
}

/**
 * Functopn for generating report element.
 */
function smr_core_modules(&$arr_report) {

  $arr_site_report = smr_get_general_site_detail();
  if (empty($arr_site_report['modules']['core'])) {
    return FALSE;
  }

  $arr_col = ['Module'];
  $arr_mod_table = array();
  foreach ($arr_site_report['modules']['core'] as $module) {
    $arr_mod_table[] = array($module['module_name']);
  }

  $arr_report[] = array(
        array(
          'template_type' => 'paragraph',
          'data' => "List of core module enabled: \n",
        ),
  );

  $arr_report[] = array(
        array(
          'template_type' => 'table',
          'row_head' => $arr_col,
          'data' => $arr_mod_table,
        ),
  );
}

/**
 * Functopn for generating report element.
 */
function smr_features(&$arr_report) {

  $arr_site_report = smr_get_general_site_detail();
  if (empty($arr_site_report['features'])) {
    return FALSE;
  }

  $arr_col = ['Feature', 'Overridden status'];
  $arr_feature_table = array();
  foreach ($arr_site_report['features'] as $feature) {
    $arr_feature_table[] = array(
      $feature['feature'],
      $feature['state'],
    );
  }

  $arr_report[] = array(
        array(
          'template_type' => 'paragraph',
          'data' => "List of enabled features:\n",
        ),
  );

  $arr_report[] = array(
        array(
          'template_type' => 'table',
          'row_head' => $arr_col,
          'data' => $arr_feature_table,
        ),
  );
}

/**
 * Functopn for generating report element.
 */
function smr_additional_information(&$arr_report) {

  $arr_site_report = smr_get_general_site_detail();
  if (empty($arr_site_report)) {
    return FALSE;
  }

  $arr_report[] = array(
        array(
          'template_type' => 'paragraph',
          'data' => "Usage of the following modules require further check on the site.\n",
        ),
  );
  $flag = count($module_sp = array_intersect(array('views_bulk_operations'), $arr_site_report['specified_module_check'])) > 0 ? 'Y' : 'N';
  $module_sp = $flag == 'Y' ? " (" . implode(', ', $module_sp) . ')' : '';
  $arr_additional_information[] = array(
    'template_type' => 'line_with_value',
    'label' => '1. Views bulk operations',
    'value' => $flag . $module_sp,
  );

  $flag = count($module_sp = array_intersect(array('aet', 'token'), $arr_site_report['specified_module_check'])) > 0 ? 'Y' : 'N';
  $module_sp = $flag == 'Y' ? " (" . implode(', ', $module_sp) . ')' : '';
  $arr_additional_information[] = array(
    'template_type' => 'line_with_value',
    'label' => '2. Aet or other token related modules',
    'value' => $flag . $module_sp,
  );

  $flag = count($module_sp = array_intersect(array('css_injector'), $arr_site_report['specified_module_check'])) > 0 ? 'Y' : 'N';
  $module_sp = $flag == 'Y' ? " (" . implode(', ', $module_sp) . ')' : '';
  $arr_additional_information[] = array(
    'template_type' => 'line_with_value',
    'label' => '3. CSS Injector',
    'value' => $flag . $module_sp,
  );

  $flag = count($module_sp = array_intersect(array('ds'), $arr_site_report['specified_module_check'])) > 0 ? 'Y' : 'N';
  $module_sp = $flag == 'Y' ? " (" . implode(', ', $module_sp) . ')' : '';
  $arr_additional_information[] = array(
    'template_type' => 'line_with_value',
    'label' => '4. Display Suite',
    'value' => $flag . $module_sp,
  );

  $flag = count($module_sp = array_intersect(array('rules'), $arr_site_report['specified_module_check'])) > 0 ? 'Y' : 'N';
  $module_sp = $flag == 'Y' ? " (" . implode(', ', $module_sp) . ')' : '';
  $arr_additional_information[] = array(
    'template_type' => 'line_with_value',
    'label' => '5. Rules',
    'value' => $flag . $module_sp,
  );

  $flag = count($module_sp = array_intersect(array('php'), $arr_site_report['specified_module_check'])) > 0 ? 'Y' : 'N';
  $module_sp = $flag == 'Y' ? " (" . implode(', ', $module_sp) . ')' : '';
  $arr_additional_information[] = array(
    'template_type' => 'line_with_value',
    'label' => '6. PHP filter',
    'value' => $flag . $module_sp,
  );

  $flag = count($module_sp = array_intersect(array('feeds'), $arr_site_report['specified_module_check'])) > 0 ? 'Y' : 'N';
  $module_sp = $flag == 'Y' ? " (" . implode(', ', $module_sp) . ')' : '';
  $arr_additional_information[] = array(
    'template_type' => 'line_with_value',
    'label' => '7. Feeds',
    'value' => $flag . $module_sp,
  );

  $flag = count($module_sp = array_intersect(array('job_scheduler'), $arr_site_report['specified_module_check'])) > 0 ? 'Y' : 'N';
  $module_sp = $flag == 'Y' ? " (" . implode(', ', $module_sp) . ')' : '';
  $arr_additional_information[] = array(
    'template_type' => 'line_with_value',
    'label' => '8. Job Scheduler',
    'value' => $flag . $module_sp,
  );

  $flag = count($module_sp = array_intersect(array('js_injector'), $arr_site_report['specified_module_check'])) > 0 ? 'Y' : 'N';
  $module_sp = $flag == 'Y' ? " (" . implode(', ', $module_sp) . ')' : '';
  $arr_additional_information[] = array(
    'template_type' => 'line_with_value',
    'label' => '9. JS Injector',
    'value' => $flag . $module_sp,
  );

  $flag = count($module_sp = array_intersect(array('markup'), $arr_site_report['specified_module_check'])) > 0 ? 'Y' : 'N';
  $module_sp = $flag == 'Y' ? " (" . implode(', ', $module_sp) . ')' : '';
  $arr_additional_information[] = array(
    'template_type' => 'line_with_value',
    'label' => '10. Markup',
    'value' => $flag . $module_sp,
  );

  $flag = count($module_sp = array_intersect(array('panels', 'panelizer'), $arr_site_report['specified_module_check'])) > 0 ? 'Y' : 'N';
  $module_sp = $flag == 'Y' ? " (" . implode(', ', $module_sp) . ')' : '';
  $arr_additional_information[] = array(
    'template_type' => 'line_with_value',
    'label' => '11. Panels (any panel related modules - minipanels etc)',
    'value' => $flag . $module_sp,
  );

  $flag = count($module_sp = array_intersect(array('rest_server', 'restful'), $arr_site_report['specified_module_check'])) > 0 ? 'Y' : 'N';
  $module_sp = $flag == 'Y' ? " (" . implode(', ', $module_sp) . ')' : '';
  $arr_additional_information[] = array(
    'template_type' => 'line_with_value',
    'label' => '12. Rest server or any REST modules, services etc.',
    'value' => $flag . $module_sp,
  );

  $flag = count($module_sp = array_intersect(array('ultimate_cron'), $arr_site_report['specified_module_check'])) > 0 ? 'Y' : 'N';
  $module_sp = $flag == 'Y' ? " (" . implode(', ', $module_sp) . ')' : '';
  $arr_additional_information[] = array(
    'template_type' => 'line_with_value',
    'label' => '13. Ultimate cron',
    'value' => $flag . $module_sp,
  );

  $flag = count($module_sp = array_intersect(array('workbench', 'workflow'), $arr_site_report['specified_module_check'])) > 0 ? 'Y' : 'N';
  $module_sp = $flag == 'Y' ? " (" . implode(', ', $module_sp) . ')' : '';
  $arr_additional_information[] = array(
    'template_type' => 'line_with_value',
    'label' => '14. Workbench/workflow',
    'value' => $flag . $module_sp,
  );

  $arr_report[] = array(
        array(
          'template_type' => 'line_with_value_list',
          'data' => $arr_additional_information,
        ),
  );
}

/**
 * Functopn for generating report element.
 */
function smr_theme(&$arr_report) {

  $arr_site_report = smr_get_general_site_detail();
  if (empty($arr_site_report)) {
    return FALSE;
  }

  $arr_report[] = array(
        array(
          'template_type' => 'line_with_value_list',
          'data' => array(
                array(
                  'label' => 'Name of the theme used',
                  'value' => $arr_site_report['theme']['default']['mech_name'],
                ),
                array(
                  'label' => 'Admin theme used',
                  'value' => $arr_site_report['theme']['active_admin_theme'],
                ),
          ),
        ),
  );
}

/**
 * Functopn for generating report element.
 */
function smr_list_of_templates_in_the_custom_theme(&$arr_report) {

  $arr_site_report = smr_get_general_site_detail();
  if (empty($arr_site_report['theme']['default']['tplfiles']['files'])) {
    return FALSE;
  }

  $arr_col = ['Template Files'];
  $arr_tpl_files = str_replace(".php", ".php$#", $arr_site_report['theme']['default']['tplfiles']['files']);
  $arr_tpl_files = explode('$#', $arr_tpl_files);
  $row_table = array();
  foreach ($arr_tpl_files as $key => $temp_file) {
    if (!empty($temp_file = trim($temp_file))) {
      $row_table[$key][] = $temp_file;
    }
  }

  $arr_report[] = array(
        array(
          'template_type' => 'table',
          'row_head' => $arr_col,
          'data' => $row_table,
        ),
  );
}

/**
 * Functopn for generating report element.
 */
function smr_list_of_js_files_included(&$arr_report) {

  $arr_site_report = smr_get_general_site_detail();
  if (empty($arr_site_report['theme']['default']['js'])) {
    return FALSE;
  }

  $arr_col = ['JS files (from theme.info)'];
  $row_table = array();
  foreach ($arr_site_report['theme']['default']['js'] as $key => $val) {
    $row_table[$key][] = $val;
  }

  $arr_report[] = array(
        array(
          'template_type' => 'paragraph',
          'data' => "List of JS files included:\n",
        ),
  );

  $arr_report[] = array(
        array(
          'template_type' => 'table',
          'row_head' => $arr_col,
          'data' => $row_table,
        ),
  );
}

/**
 * Functopn for generating report element.
 */
function smr_list_of_css_files_included(&$arr_report) {

  $arr_site_report = smr_get_general_site_detail();
  if (empty($arr_site_report['theme']['default']['css'])) {
    return FALSE;
  }

  $arr_col = ['CSS files (from theme.info)'];
  $row_table = array();
  foreach ($arr_site_report['theme']['default']['css'] as $key => $val) {
    $row_table[$key][] = $val;
  }

  $arr_report[] = array(
        array(
          'template_type' => 'paragraph',
          'data' => "List of CSS files included:\n",
        ),
  );

  $arr_report[] = array(
        array(
          'template_type' => 'table',
          'row_head' => $arr_col,
          'data' => $row_table,
        ),
  );
}

/**
 * Functopn for generating report element.
 */
function smr_templates_php(&$arr_report) {

  $arr_site_report = smr_get_general_site_detail();
  if (empty($arr_site_report)) {
    return FALSE;
  }

  $arr_report[] = array(
        array(
          'template_type' => 'line_with_value',
          'label' => 'Lines of code',
          'value' => $arr_site_report['theme']['default']['template.php']['lines_count'],
        ),
  );
}

/**
 * Functopn for generating report element.
 */
function smr_list_of_functions_used(&$arr_report) {

  $arr_site_report = smr_get_general_site_detail();
  if (empty($arr_site_report['theme']['default']['template.php']['file_function'])) {
    return FALSE;
  }

  $arr_col = ['Functions'];
  $row_table = array();
  foreach ($arr_site_report['theme']['default']['template.php']['file_function'] as $key => $functions) {
    $row_table[$key][] = $functions;
  }

  $arr_report[] = array(
        array(
          'template_type' => 'paragraph',
          'data' => "List of functions used:\n",
        ),
  );

  $arr_report[] = array(
        array(
          'template_type' => 'table',
          'row_head' => $arr_col,
          'data' => $row_table,
        ),
  );
}

/**
 * Functopn for generating report element.
 */
function smr_users(&$arr_report) {

  $arr_site_report = smr_get_general_site_detail();
  if (empty($arr_site_report['user']['roles'])) {
    return FALSE;
  }

  $arr_col = ['Role', 'Total User'];
  $row_table = array();
  $key = 0;
  foreach ($arr_site_report['user']['roles'] as $rid => $role) {
    $row_table[$key][] = $role['name'];
    $row_table[$key][] = $role['user_count'];
    $key++;
  }

  $arr_report[] = array(
        array(
          'template_type' => 'table',
          'row_head' => $arr_col,
          'data' => $row_table,
        ),
  );

  $arr_report[] = array(
        array(
          'template_type' => 'paragraph',
          'data' => "List of permissions:\n",
        ),
  );

  $arr_col = ['Permission'];
  $row_table = array();
  foreach (array_keys($arr_site_report['user']['permissions']) as $permission) {
    $row_table[$key][] = $permission;
  }
  $arr_report[] = array(
        array(
          'template_type' => 'table',
          'row_head' => $arr_col,
          'data' => $row_table,
        ),
  );
}

/**
 * Functopn for generating report element.
 */
function smr_crons(&$arr_report) {

  $arr_site_report = smr_get_general_site_detail();
  if (empty($arr_site_report['user']['roles'])) {
    return FALSE;
  }

  $arr_report[] = array(
        array(
          'template_type' => 'paragraph',
          'data' => "List of custom cron jobs :\n",
        ),
  );

  $arr_col = ['Cron'];
  $row_table = array();
  $key = 0;
  foreach ($arr_site_report['crons_using_modules'] as $cron) {

    if (is_array($arr_site_report['modules']['custom'])) {
      if (in_array($cron, array_keys($arr_site_report['modules']['custom']))) {
        $row_table[$key][] = $cron;
        $key++;
      }
    }
  }
  $arr_report[] = array(
        array(
          'template_type' => 'table',
          'row_head' => $arr_col,
          'data' => $row_table,
        ),
  );
}

/**
 * Functopn for generating report element.
 */
function smr_summary(&$arr_report) {

  $arr_site_report = smr_get_general_site_detail();
  if (empty($arr_site_report)) {
    return FALSE;
  }

  $arr_report[] = array(
        array(
          'template_type' => 'summary_list',
          'data' => array(
                array(
                  'label' => 'Number of published nodes',
                  'value' => $arr_site_report['total_published_node_count'],
                ),
                array(
                  'label' => 'Number of custom modules enabled',
                  'value' => count($arr_site_report['modules']['custom']),
                ),
                array(
                  'label' => 'Number of lines of custom code',
                  'value' => array_sum($arr_site_report['modules']['custom']['lines_of_code']),
                ),
                array(
                  'label' => 'Number of features enabled',
                  'value' => count($arr_site_report['features']),
                ),
                array(
                  'label' => 'Number of views',
                  'value' => count($arr_site_report['views']),
                ),
                array(
                  'label' => 'Number of blocks',
                  'value' => count($arr_site_report['theme']['default']['block']),
                ),
                array(
                  'label' => 'Active theme',
                  'value' => $arr_site_report['theme']['default']['mech_name'],
                ),
                array(
                  'label' => 'Base theme',
                  'value' => $arr_site_report['theme']['default']['base_theme'],
                ),
          ),
        ),
  );
}

/**
 * Functopn for generating report element.
 */
function smr_basic_information(&$arr_report) {

  $arr_site_report = smr_get_general_site_detail();
  if (empty($arr_site_report)) {
    return FALSE;
  }

  $arr_report[] = array(
        array(
          'template_type' => 'line_with_value_list',
          'data' => array(
                array(
                  'label' => 'Files directory size',
                  'value' => $arr_site_report['site_information']['file_directory_size'],
                ),
                array(
                  'label' => 'Drupal version',
                  'value' => $arr_site_report['site_information']['drupal-version'],
                ),
          ),
        ),
  );
}
