<?php

/**
 * @file
 * Help page template.
 */
?>
<?php foreach ($content as $faq_content): ?>
  <div class="accordion"><?php print render($faq_content['question']); ?></div>
  <div class="panel">
    <p><?php print render($faq_content['Answer']); ?></p>
  </div>
<?php endforeach; ?>
