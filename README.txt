Drupal Upgrade/Migration Audit

Installation steps:-

Step1: Download the module via composer.

    composer require drupal/upgrade_audit

Step2: Download the parsedown (https://github.com/erusev/parsedown/releases/tag/1.7.4) library and copy to libraries folder as below.

    sites/all/libraries/parsedown/

Step3: Download the parsedown extra (https://github.com/erusev/parsedown-extra/releases/tag/0.8.1) library and copy to libraries folder as below.

    sites/all/libraries/parsedown-extra/

Step4: Download the latest version of dompdf library (https://github.com/dompdf/dompdf/releases/tag/v2.0.1) and copy to libraries folder as below.

	sites/all/libraries/dompdf/

Generate Report steps:-

Step1: Generate Audit Report via UI:

  a) Enable the module and visit admin/reports/upgrade-audit
  
  b) Press the Generate the Audit Report button. A btach operation will run and create the migration report.

  c) Once report generated, it is ready to download in md and pdf format.

  d) If there is any content, module or other changes happen in site, click on Re-generate the Audit report button for an updated report.

Step2: Generate Audit Report via Drush command.

    drush smr md > [filepath/filename.md]


Visit the https://d7upgrade.org for more details about the audit report.
