<?php

/**
 * @file
 * Generate the pdf of audit report.
 */

namespace Dompdf;

require_once DRUPAL_ROOT . '/sites/all/libraries/dompdf/autoload.inc.php';
global $base_root;
$table_of_contetns = upgrade_audit_report_index();
$path = variable_get('file_private_path');
if (empty($path)) {
  $path = variable_get('file_public_path', 'sites/default/files');
}
$file_path = DRUPAL_ROOT . '/' . $path . '/audit-report/upgrade-audit-report.md';
$report_time = date("F d, Y", filemtime($file_path));
$site_name = variable_get('site_name', "Default site name");
$audit_report_content = '
<div class="report-cover-page">
  <h2 class="site-name"> Site Name : ' . $site_name . '
    <div class="site-url"> Site URL : <a href=' . $base_root . '>' . $base_root . '</a></div>
  </h2>
  <div class="report-date-wrapper">
  <div class = "report-date-label">On</div>
  <div class = "report-date">
  ' . $report_time . '</div>
  </div>
</div>';
//echo "<pre>";print_r($audit_report_content);exit;
$audit_report_content .= '<div class="report-main-wrapper" id="audit-report-print-area"><div class="report-table-wrapper"><div class="report-index">
' . $table_of_contetns . '
</div></div> <span class="break-page"></span>';
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $audit_report_content .= '<div class="report-content"><div id="upgrade-audit-report">';
  $audit_report_content .= $_POST['audit_report_content'];
  $audit_report_content .= '</div></div>';
}
$css = drupal_get_path('module', 'upgrade_audit') . '/css/upgrade_audit_pdf.css';
$load_css = file_get_contents($css);
$audit_report_content .= '<style type="text/css"> ' . $load_css . '</style>';
$options = new Options();
$options->set('isPhpEnabled', 'true');
$dompdf = new Dompdf($options);
$dompdf->setOptions($options);
$dompdf->loadHtml($audit_report_content);
$dompdf->setPaper('A4', '');
$dompdf->render();
// Instantiate canvas instance.
$canvas = $dompdf->getCanvas();
$logo_path = drupal_get_path('module', 'upgrade_audit') . '/image/Audit-reportlogo.png';
$GLOBALS["logo1"] = $logo_path;
$canvas->page_script('
  if ($PAGE_NUM >= 1) {
    $font = $fontMetrics->getFont("times", "normal"); 
    $pdf->set_opacity(1,"Multiply");
    $pdf->text(550, 815, "$PAGE_NUM / $PAGE_COUNT", $font, 10, array(0,0,0));
    if ($PAGE_NUM == 1) {
      $pdf->image($GLOBALS["logo1"], 150, 280, 300, 66);
    }
    else {
      $pdf->image($GLOBALS["logo1"], 15, 10, 100, 22);
    }
    $pdf->text(15, 815, "https://d7upgrade.org", $font, 10, array(0 / 255, 116 / 255, 189 / 255));
  }
');
$dompdf->stream("upgrade-audit-report.pdf");
exit(0);
